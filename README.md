# Path of Chaos

Path of Chaos is an extensive overhaul mod for the popular survival game
7 Days to Die with a focus on improving quality of life features, immersion,
and expanding content in a meaningful way. It features new items, blocks,
recipes, prefab changes, and more, resulting in a more complex survival
experience!

One of the central features of Path of Chaos is the addition of two new biomes:
the Ash biome and the Swamp biome. These new biomes bring unique challenges and
opportunities for players, with new enemies and resources to discover. The mod
also revamps the existing biomes in the game, making them more diverse and
immersive with new terrain features, such as rocky outcroppings, caves, and
unique flora and fauna, to make each biome feel more distinct.

## Versioning

We use [semantic versioning] because it makes things
simple. Versions consist of three numbers (from left to right): major version,
minor version, and patch. When the first number (the **major version**) is
increased, it indicates that the release contains backward-incompatible changes
which will require a game restart. When the major version is increased, the
minor version and patch are reset to zero. When the second number (the
**minor version**) is increased, it indicates that the release contains
backward-compatible changes which will not require a game restart. When the
third number (the **patch**) is increased, it indicates that the release
contains only bug fixes.

## Installation

### Install from GitLab

You can download any version of Path of Chaos from the [Releases page].

[semantic versioning]: https://semver.org
[Releases page]: https://gitlab.com/moonlightchaosstudios/pathofchaos/pathofchaos/-/releases
