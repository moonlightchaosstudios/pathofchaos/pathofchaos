# Bug Report

## Describe the bug

A clear and concise description of what the bug is. Please be as descriptive as
possible; issues lacking detail, or for any other reason than to report a bug,
may be closed without action.

## To reproduce

Steps to reproduce the behavior:

1. Go to '...'
2. Click on '....'
3. See error

## Screenshots

If applicable, add screenshots to help explain your problem.

## Expected behavior

A clear and concise description of what you expected to happen.

/label ~Tag::Bug ~Status::Icebox
