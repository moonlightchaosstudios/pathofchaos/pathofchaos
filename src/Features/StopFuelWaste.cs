namespace PathOfChaos.Harmony;

[HarmonyPatch(typeof(TileEntityWorkstation), "HandleFuel")]
public class SetStationState
{
	public static bool Prefix(TileEntityWorkstation __instance)
	{
    // If the Station is not crafting anything, then we turn off the station.
		if(!__instance.IsCrafting){
			__instance.IsBurning = false;
    }

		return true;
	}
}
